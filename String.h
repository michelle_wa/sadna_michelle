#ifndef STRING_H
#define STRING_H

#include "Sequence.h"

class String : public Sequence
{
private:
	std::string _str;

public:
	String(const std::string& str);
	bool isPrintable() const;
	std::string toString() const;
};

#endif // STRING_H