#include "type.h"

Type::Type()
{
	_isTemp = false;
}
bool Type::get_isTemp() const
{
	return _isTemp;
}
void Type::set_isTemp(const bool isTemp)
{
	_isTemp = isTemp;
}