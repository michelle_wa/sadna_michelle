#include "type.h"
#include "InterperterException.h"
#include "IndentationException.h"
#include "parser.h"
#include <iostream>
#include "SyntaxException.h"
#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "Michelle"


int main(int argc, char **argv)
{
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string input_string;

	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);

	while (input_string != "quit()")
	{
		// prasing command
		try
		{
			Parser::parseString(input_string);
		}
		catch (const IndentationException &e)
		{
			std::cout << e.what();
		}
		catch (SyntaxException &e)
		{
			std::cout<<e.what()<<std::endl;
		}

		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}

	return 0;
}


