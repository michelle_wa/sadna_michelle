#ifndef TYPE_H
#define TYPE_H
#include <string>

class Type
{
private:
	bool _isTemp;
public:
	Type();
	bool get_isTemp() const;
	void set_isTemp(const bool isTemp);

	virtual bool isPrintable() const = 0;
	virtual std::string toString() const = 0;
};

#endif //TYPE_H
