#ifndef VOID_H
#define VOID_H

#include "type.h"
#include <iostream>

class Void : public Type
{
public:
	Void();
	bool isPrintable() const;
	std::string toString() const;
};


#endif // VOID_H