#include "Boolean.h"

Boolean::Boolean(bool b)
{
	this->_Boolean = b;
}
bool Boolean::isPrintable() const
{
	return true;
}
std::string Boolean::toString() const
{
	if (this->_Boolean)
	{
		return "True";
	}
	else
	{
		return "False";
	}
}
