#ifndef BOOLEAN_H
#define BOOLEAN_H

#include "type.h"
#include <iostream>
class Boolean : public Type
{
private:
	bool _Boolean;
public:
	Boolean(bool b);
	bool isPrintable() const;
	std::string toString() const;
};


#endif // BOOLEAN_H