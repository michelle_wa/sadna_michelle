#ifndef INTEGER_H
#define INTEGER_H

#include "type.h"
#include <iostream>
class Integer : public Type
{
private:
	int _integer;
public:
	Integer(int n);
	bool isPrintable() const;
	std::string toString() const;

};


#endif // INTEGER_H