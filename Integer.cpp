#include "Integer.h"
#include <string>   

Integer::Integer(int n)
{
	_integer = n;
}
bool Integer::isPrintable() const
{
	return true;
}
std::string Integer::toString() const
{
	return std::to_string(_integer);
}
