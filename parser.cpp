#include "parser.h"

/*#include "IndentationException.h"
#include "InterperterException.h"
#include "Integer.h"
#include "Boolean.h"
#include "String.h"
#include "Void.h"
#include "type.h"
#include "String.h"*/
#include "Integer.h"
#include "Boolean.h"
#include "String.h"
#include <iostream>
#include "SyntaxException.h"


Type* Parser::parseString(std::string str) throw()
{
	if (!str.rfind(" ", 0) || !str.rfind('\t', 0))
	{
		throw IndentationException();
	}
	Type* type = getType(str);
	if (type)
	{
		if (type->isPrintable())
		{
			std::cout << type->toString() << std::endl;
		}
	}
	else
	{
		throw SyntaxException();
	}


}



Type* Parser::getType(const std::string& str)
{
	Type* toReturn = nullptr;
	if (Helper::isInteger(str))
	{
		toReturn = new Integer(std::stoi(str));
	}
	else if (Helper::isBoolean(str))
	{
		toReturn = new Boolean(str == "True");
	}
	else if (Helper::isString(str))
	{		
		toReturn = new String(str.substr(1, str.size() - 2));
	}
	if (toReturn != nullptr)
	{
		toReturn->set_isTemp(true);
	}
	return toReturn;  // void
}


