#ifndef PARSER_H
#define PARSER_H

#include "IndentationException.h"
#include "InterperterException.h"
#include "type.h"
#include "Helper.h"
#include "String.h"
#include "Integer.h"
#include "Boolean.h"
#include "Void.h"
#include <string>
#include <unordered_map>
#include <iostream>

class Parser
{
public:
	static Type* parseString(std::string str) throw();
	static Type* getType(const std::string& str) throw();
private:

};
#endif //PARSER_H















/*
#pragma once

#include "InterperterException.h"
#include "type.h"
#include "Helper.h"
#include <string>
#include <unordered_map>
#include <iostream>
#include <sstream>
#include <unordered_map>


class Parser
{
static std::unordered_map<std::string, Type*> _variables;

static bool isLegalVarName(const std::string& str);
static bool makeAssignment(const std::string& str);
static Type* getVariableValue(const std::string &str);

public:
static Type* parseString(const std::string& str);
static Type* getType(const std::string& str);
static void clearMemory();
};

*/
