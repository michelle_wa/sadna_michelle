#include <iostream>
#include "String.h"

String::String(const std::string& str)
{
	this->_str = str;
}

bool String::isPrintable() const
{
	return true;
}

std::string String::toString() const
{
	if (this->_str.find("'") != std::string::npos)
	{
		
		return "\"" + this->_str + "\"";
	}
	else
	{
		return "'" + this->_str + "'";
	}
}
