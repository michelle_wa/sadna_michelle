#ifndef SEQUENCE_H
#define SEQUENCE_H

#include <iostream>
#include "type.h"

class Sequence : public Type
{
public:
	Sequence();
	virtual bool isPrintable() const = 0;
	virtual std::string toString() const = 0;
};

#endif // SEQUENCE_H